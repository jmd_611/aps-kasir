## Project 2 - Aplikasi Kasir (Laravel Project) From SIPD JABAR
### APLIKASI KASIR

#### Anda diminta untuk membuat aplikasi kasir sederhana yang mencakup fitur:

- Mencatat transaksi pembelian barang
- Menampilkan daftar transaksi pembelian barang
- Authentikasi login pengguna
- Pengaturan data pengguna
- Pengaturan data master produk
- Jenis barang didefinisikan dengan sebuah tabel yang terdiri dari field berikut:
- master_barang = id, nama_barang, harga_satuan, created_at, updated_at

#### Transaksi pembelian didefinisikan dengan 2 buah table berikut:

- transaksi_pembelian = id, total_harga, created_at, updated_at
- transaksi_pembelian_barang = id, transaksi_pembelian_id, master_barang_id, jumlah, harga_satuan


#### Barang awal yang dibuat langsung dengan DB Seeder:

- Sabun batang, harga 3000, ID: 1
- Mi Instan, harga 2000, ID: 2
- Pensil, harga 1000, ID: 3
- Kopi sachet, harga 1500, ID: 4
- Air minum galon, harga 20000, ID: 5


## 1. Mencatat transaksi pembelian barang

Buatlah form untuk mencatat barang apa saja yang dibeli beserta jumlahnya. Munculkan subtotal harga per barang dan harga total transaksi tersebut. Ketika disimpan, masukkan data ke transaksi. Form transaksi mengandung: 

   - Form Barang, field-nya ID, Kuantitas (number, diatas 0), dan subtotal (hitung harga barang dikalikan dengan kuantitas)
   - Pemilihan barang menggunakan dropdown dari tabel master_barang
   - Form pada poin 1.b menggunakan select yang bisa di-search
   - Jumlah barang yang dibeli bisa ditambahkan secara dinamis. Jadi, 1 Transaksi bisa mengandung 1 atau lebih barang yang dibeli
   - Informasi Total, menjumlahkan semua subtotal dari form barang
   - Tombol simpan, untuk menyimpan entri transaksi
   - Tombol print, untuk mencetak struk transaksi dalam bentuk tekstual untuk dicetak ke thermal printer

## 2. Menampilkan daftar transaksi pembelian barang
- Tampilkan tabel yang berisi: ID transaksi, waktu transaksi (dalam format: Senin, 11 Oktober 2021 07:00), total harga  (dalam format Rp 35.500 rata kanan)
- Buat tombol detail pada masing-masing transaksi. Tampilkan detail transaksi. Tampilkan tabel yang berisi barang yang dibeli pada transaksi tersebut, total transaksi, dan waktu transaksi dilakukan
- Buat fitur download transaksi ke PDF (1 transaksi 1 pdf). Template dibebaskan
- Buat fitur pencarian berdasarkan tanggal pembelian barang
- Buat fitur pencarian berdasarkan total harga (di bawah nilai tertentu, di atas nilai tertentu)
- Buat fitur pencarian berdasarkan kombinasi barang yang dibeli (misal, cari transaksi yang membeli mi instan dan kopi sachet)
- Buat fitur pengurutan berdasarkan waktu transaksi atau total harga
## 3. Authentikasi login pengguna

Pengguna harus login ke aplikasi kasir sebelum dapat melakukan apapun. Role terbagi menjadi 2, yaitu kasir dan admin kasir. Kasir hanya dapat mencatat transaksi dan mengakses data penjualan pada hari itu saja. Sedangkan Admin kasir, dapat melakukan semua hal.

## 4. Pengaturan data penguna

Buatlah fitur CRUD data pengguna aplikasi kasir. Tambahkan fitur pencarian berdasarkan role, email, dan username.

## 5. Pengaturan master produk
Buatlah fitur CRUD data master produk. Tambahkan fitur pencarian berdasarkan nama barang dan harga (di bawah, di atas, atau sama dengan angka tertentu).

Gunakan layout template SB Admin 2 [https://startbootstrap.com/theme/sb-admin-2]

Anda boleh menambahkan kreativitas fitur relevan / UI / UX apapun yang menunjang operasionalitas dan kemanfaatan aplikasi kasir. Anda disarankan meminta teman dekat / rekan JCC Anda untuk mengecek dan mengetes fitur pada aplikasi Anda untuk mendapatkan feedback yang tepat dalam mengerjakan aplikasi kasir ini.

Aplikasi dibuat menggunakan laravel versi 8.0. Anda dibebaskan menggunakan library PHP, CSS, dan JS pendukung apapun. Anda diperbolehkan melihat referensi buku / online dan kerjakan secara mandiri namun boleh bertanya kepada orang lain bila perlu. Kumpulkan source code dengan cara push ke gitlab lalu invite GitLab.com @kamilersz sebagai developer.

Pengumpulan di website JCC

Kumpulkan link gitlab pada pengumpulan Tugas 
Project 2 Aplikasi Kasir - Laravel


*---=== SELAMAT MENGERJAKAN ===---*

<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[Many](https://www.many.co.uk)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- **[OP.GG](https://op.gg)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
