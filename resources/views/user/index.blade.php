@extends('layouts.app')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">User</h1>
    </div>
    <div class="card">
        <div class="card-header text-right">
            <a href="{{ route('penggunaCreate') }}" class="btn btn-primary btn-md">Tambah User</a>
        </div>
        <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover mt-3" id="dataTable" width="100%" cellspacing="0">
            <thead class="bg-gray-600 text-white">
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Role</th>
                    <th style="text-align:center; vertical-align: inherit; width: 150px;">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($users as $key => $item)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->email }}</td>
                        <td
                        @if ($item->role->id == '1')
                        {
                            @class(['text-primary' ,'font-bold' => true])
                        } @else {
                        @class(['text-info' ,'font-bold' => false])
                        }
                        @endif
                        >
                            {{ $item->role->nama }}
                        </td>
                        <td>
                            <div class="d-flex flex-wrap flex-column flex-md-row justify-center">
                                <form action="{{ route('penggunaDestroy',$item->id) }}" method="post">
                                    @csrf
                                    <a href="{{ route('penggunaShow',$item->id )}}" class="btn btn-info btn-detail btn-sm m-1">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                    </a>
                                    <a href="{{ route('penggunaEdit',$item->id )}}" class="btn btn-warning btn-sm m-1">
                                        <i class="fa fa-edit" aria-hidden="true"></i>
                                    </a>
                                    @method('delete')
                                    <button type="submit" class="btn btn-info btn-danger btn-sm m-1 show_confirm" id="btn-hapus">
                                        <i class="fa fa-trash fa-sm"></i>
                                    </button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @empty
                    <p>Data User Kosong!</p>
                @endforelse
            </tbody>
            <tfoot>
            </tfoot>
        </table>
    </div>
    </div>
</div>
</div>
<!-- /.container-fluid -->
@endsection

@push('scripts')
<link href="{{ asset('sbadmin/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<!-- Page level plugins -->
<script src="{{ asset('sbadmin/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('sbadmin/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
{{-- sweet altert --}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>


<script>
$(document).ready(function() {
    $('#dataTable').DataTable();
    $('[data-toggle="tooltip"]').tooltip();

    $('.show_confirm').click(function(event) {
        event.preventDefault();
        var form = $(this).closest("form");
        Swal.fire({
            title: 'Yakin Bakal Hapus Data ini?',
            text: "JIka Anda Yakin Data Akan Hilang..! ⚠️",
            type: 'warning',
            showCancelButton: true,
            // Background color of the "Confirm"-button. The default color is #3085d6
            confirmButtonColor: 'LightSeaGreen',
            // Background color of the "Cancel"-button. The default color is #aaa
            cancelButtonColor: 'Crimson',
            confirmButtonText: 'Yakin lah..',
        }).then((result) => {
            if (result.value) {
                Swal.fire({
                    type: 'success',
                    title: 'Berhasil dihapus.',
                    timer: 5000,
                    showCancelButton: false,
                    showConfirmButton: false,
                })
            form.submit();
            }
        })
    });
});
</script>

@endpush