@extends('layouts.app')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex">
        <h1 class="h3 mb-0 text-gray-800">Create User</h1>
    </div>
    <div class="card">
        <div class="card-header"></div>
        <div class="card-body">
            <form action="{{ '/home/pengguna'}}" method="post">
                @csrf                
                <div class="form-group">
                    <label for="name">Nama Lengkap</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
                </div>
                @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="{{ old('username') }}">
                </div>
                @error('username')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="email">E-Mail</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{ old('email') }}">
                </div>
                @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name="password" value="{{ old('password') }}">
                </div>
                @error('password')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="password_confirmation">Ketik Lagi Password</label>
                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}">
                </div>
                @error('password_confirmation')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="role_id">Role</label>
                    <select name="role_id" id="role_id" class="form-control">
                        <option value="">---Pilih Role---</option>
                        @foreach ($roles as $item)
                            @if (old('role_id') == $item->id)
                            { 
                                <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
                            } @else {
                                <option value="{{ $item->id }}">{{ $item->nama }}</option>
                            }
                            @endif
                        @endforeach
                    </select>
                </div>
                @error('role_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <a href="{{ '/home/pengguna'}}" class="btn btn-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i>Simpan</button>
            </form>
        </div>
        <div class="card-footer"></div>
    </div>
</div>
@endsection