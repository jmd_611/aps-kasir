@extends('layouts.app')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex">
        <h1 class="h3 mb-0 text-gray-800">Edit Data User {{ $user->name }}</h1>
    </div>
    <div class="card">
        <div class="card-header"></div>
        <div class="card-body">
                <div class="form-group">
                    <label for="name">Nama Lengkap</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}" disabled>
                </div>
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="{{ $user->username }}" disabled>
                </div>
                <div class="form-group">
                    <label for="email">E-Mail</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{ $user->email }}" disabled>
                </div>
                <div class="form-group">
                    <label for="role_id">Role</label>
                    <select name="role_id" id="role_id" class="form-control" disabled>
                        <option value="">---Pilih Role---</option>
                        @foreach ($roles as $item)
                            @if ($user->role_id == $item->id)
                            { 
                                <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
                            } @else {
                                <option value="{{ $item->id }}">{{ $item->nama }}</option>
                            }
                            @endif
                        @endforeach
                    </select>
                </div>
                @error('role_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <a href="{{ '/home/pengguna'}}" class="btn btn-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>

                <a href="{{ '/home/pengguna/edit/'.$user->id}}" class="btn btn-info"><i class="fa fa-arrow-left" aria-hidden="true"></i> Ubah Data</a>
            </form>
        </div>
        <div class="card-footer"></div>
    </div>
</div>
@endsection