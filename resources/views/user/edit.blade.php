@extends('layouts.app')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex">
        <h1 class="h3 mb-0 text-gray-800">Edit Data User {{ $user->name }}</h1>
    </div>
    <div class="card">
        <div class="card-header"></div>
        <div class="card-body">
            <form action="/home/pengguna/{{ $user->id }}" method="post">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Nama Lengkap</label>
                    <input type="text" class="form-control" id="name" name="name" value="{{ $user->name }}">
                </div>
                @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="username">Username</label>
                    <input type="text" class="form-control" id="username" name="username" value="{{ $user->username }}">
                </div>
                @error('username')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="email">E-Mail</label>
                    <input type="text" class="form-control" id="email" name="email" value="{{ $user->email }}">
                </div>
                @error('email')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="form-group">
                    <label for="role_id">Role</label>
                    <select name="role_id" id="role_id" class="form-control">
                        <option value="">---Pilih Role---</option>
                        @foreach ($roles as $item)
                            @if ($user->role_id == $item->id)
                            { 
                                <option value="{{ $item->id }}" selected>{{ $item->nama }}</option>
                            } @else {
                                <option value="{{ $item->id }}">{{ $item->nama }}</option>
                            }
                            @endif
                        @endforeach
                    </select>
                </div>
                @error('role_id')
                    <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <a href="{{ '/home/pengguna'}}" class="btn btn-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                <button type="submit" class="btn btn-primary show_confirm"><i class="fa fa-save" aria-hidden="true"></i>
                    Simpan</button>
            </form>
        </div>
        <div class="card-footer"></div>
    </div>
</div>
@endsection

@push('scripts')
    
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script type="text/javascript">
    $('.show_confirm').click(function(event) {
        var form = $(this).closest("form");
        var name = document.getElementById('name').value;
        event.preventDefault();
        Swal.fire({
            title: 'Yakin Bakal Edit Data ini?',
            text: "JIka Anda Yakin Data Akan Disimpan..! ⚠️",
            type: 'warning',
            showCancelButton: true,
            // Background color of the "Confirm"-button. The default color is #3085d6
            confirmButtonColor: 'LightSeaGreen',
            // Background color of the "Cancel"-button. The default color is #aaa
            cancelButtonColor: 'Crimson',
            confirmButtonText: 'Yakin lah..'
        }).then((result) => {
            if (result.value) {
                Swal.fire({
                    type: 'success',
                    title: 'Data ' + name + 'Telah Diubah.',
                    timer: 5000,
                    showCancelButton: false,
                    showConfirmButton: false
                })
                form.submit();
            }
        });
    });
</script>
@endpush