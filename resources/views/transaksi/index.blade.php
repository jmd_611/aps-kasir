@extends('layouts.app')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">User</h1>
    </div>
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-lg-6 align-content-between">
                        <a href="{{ route('transaksiBarangCreate') }}" type="submit" class="btn btn-outline-primary btn-md" id="createNewTransaksiBarang" onclick="createNewTransaksiBarang()">
                            <i class="fa fa-plus " aria-hidden="true"></i> Transaksi Pembelian Barang
                        </a>
                </div>
                <div class="col-lg-6 align-content-lg-end" >
                    <table border="0" cellspacing="5" cellpadding="5">
                        <tbody
                            ><tr>
                                <td>Minimum date:</td>
                                <td><input type="text" id="min" name="min"></td>
                            </tr>
                            <tr>
                                <td>Maximum date:</td>
                                <td><input type="text" id="max" name="max"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
        <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover mt-3 display" id="dataTable" width="100%" cellspacing="0">
            <thead class="bg-gray-600 text-white">
                <tr>
                    <th class="text-center">No</th>
                    <th class="text-center">Tanggal Transaksi</th>
                    <th class="text-center">Total Harga</th>
                    <th style="text-align:center; vertical-align: inherit; width: 150px;">Action</th>
                </tr>
            </thead>
            <tbody>
              {{-- {{ dd($transaksi) }} --}}
              @forelse ($transaksi as $key => $temp)
              <tr>
                
                  <td class="text-center">{{ $key + 1}}</td>
                  <td class="text-center">{{ $temp->created_at }}</td>
                  <td class="text-right">@currency($temp->total_harga)</td>
                  <td>
                    <div class="d-flex flex-wrap flex-column flex-md-row justify-center">
                      <a id="detailBarang" class="btn btn-info btn-detail btn-sm m-1" data-id="{{ $temp->id }}" data-toggle="modal" data-target="#modal-id">
                          <i class="fa fa-info-circle" aria-hidden="true"></i>
                      </a>
                      <a id="updateBarang" class="btn btn-warning btn-sm m-1" data-id="{{ $temp->id }}" data-toggle="modal" data-target="#modal-id">
                          <i class="fa fa-edit" aria-hidden="true"></i>
                      </a>
                      <a id="hapusBarang" class="btn btn-info btn-danger btn-sm m-1" data-id="{{ $temp->id }}">
                          <i class="fa fa-trash fa-sm"></i>
                      </a>
                  </div>  
                  </td>
                  </td>
              </tr>
          @empty
              <p>Data User Kosong!</p>
          @endforelse
            </tbody>
            <tfoot>
            </tfoot>
        </table>
    </div>
    </div>
</div>
</div>
<!-- /.container-fluid -->

@endsection

@push('scripts')

<link href="{{ asset('sbadmin/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<script src="{{ asset('sbadmin/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('sbadmin/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
{{-- sweet altert --}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<script>
$(document).ready(function() {
  // DataTables initialisation
  var table = $('#dataTable').DataTable();
});

function createNewTransaksiBarang()
{
    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{ route('transaksiStore') }}",
        type: "POST",
        data: {
            harga_barang: '0',
        },
        cache: false,
        success: function(data){
            console.log(data);
            var data = JSON.parse(data);
            if(data.statusCode==200){
                    console.log('done');
                }
                else if(data.statusCode==201){
                    alert("Error occured !");
                } 
        },
        error:function (response) {
           console.log ('kayaknya ada yang salah');
        }
    });
}

</script>

@endpush