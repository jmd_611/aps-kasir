@extends('layouts.app')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">
  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center mb-4">
    <a href="{{ route('transaksiIndex') }}" class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
      <h1 class="h3 mb-0 text-gray-800">&nbspTambah Transaksi Pembelian</h1>
      {{-- isi page --}}
  </div>
  <div class="row">
    <div class="col-md-9">
      <form action="{{ '/home/pengguna'}}" method="post">
        @csrf
        <div class="row">
          <div class="col-md-6 mb-3">
            <div class="form-group">
              <label for="master_barang_id">Nama Barang</label>
              <select  class="form-control select2" style="width: 100%;"id="master_barang_id" name="master_barang_id" onchange="pilih_harga()"></select>
            </div>
              <span class="text-danger" id="master_barang_idError"></span>
          </div>
          <div class="col-md-6 mb-3">
            <div class="form-group">
              <label for="jumlah">Jumlah Barang</label>
              <input type="text" class="form-control" id="jumlah" name="jumlah" value="{{ old('jumlah') }}" onchange="total_harga_satuan()">
            </div>
          </div> 
        </div>              
        <div class="row">
          <div class="col-md-2">
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text">ID Barang</span>
                </div>
                <input type="text" class="form-control" id="id_barang_hasil" name="id_barang_hasil" disabled >
              </div>
            </div>              
          </div>
          <div class="col-md-4">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Harga Satuan Rp.</span>
              </div>
              <input type="text" class="form-control" id="harga_satuan" name="harga_satuan" disabled >
            </div>         
          </div>
          <div class="col-md-4">
            <div class="input-group">
              <div class="input-group-prepend">
                <span class="input-group-text">Total Harga Rp.</span>
              </div>
              <input type="text" class="form-control" id="total_harga" name="total_harga" disabled >
            </div>
          </div>
          <div class="col-md-2">
            <div class="form-group">
              <input type="number" id="transaksi_pembelian_id" name="transaksi_pembelian_id" value="{{ $transaksipembelian_id }}">
              <button type="button" id="saveBtn" class="btn btn-primary btn-sm"><i class="fa Example of cart-plus fa-cart-plus" aria-hidden="true"></i>  Simpan</button>
            </form>
            </div>              
          </div>
        </div>
    </div>
    <div class="col-md-3">
      <h5 class="h3 mb-1 ext-capitalize text-muted">Total Belanja</h5>
      <input type="text" class="form-control" id="total_harga_tampil" name="total_harga_tampil" disabled value="@currency($totaltransaksipembelian->total_harga)">
    </div>
  </div>
  <div class="row">
    <div class="col-12 mt-4">
      <div class="card">
        <div class="card-body">
          <table class="table data-table">
            <thead>
              <tr>
                  <th>Nama Barang</th>
                  <th>Jumlah</th>
                  <th>Harga Satuan</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@push('scripts')

<link href="{{ asset('sbadmin/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<script src="{{ asset('sbadmin/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('sbadmin/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
{{-- sweet altert --}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<script>
  
$('#master_barang_id').select2({
  placeholder: "-- Masukan keyword barang --",
  width: 'resolve',
  minimumInputLength: 2,
  ajax : {
    url : "{{ route('barangAjaxId') }}",
    dataType : 'JSON',
    delay : 250,
    processResults: function (data) {
      return {
        results : $.map(data, function (item){
          return {
            text: item.nama_barang,
            value: item.harga_satuan,
            id: item.id,
          }
        })
      };
    },
    cache: true
  }
});

function pilih_harga()
{
  var master_barang_id = $('#master_barang_id').val();
  document.getElementById('id_barang_hasil').value = master_barang_id;
  event.preventDefault();
      var id = $(this).data('id');
      var url = '{{ route("barangAjaxHarga", ":id") }}';
      url = url.replace(':id', master_barang_id);
      $.get(url, function (data) {
          $('#harga_satuan').val(data[0].harga_satuan);
          console.log(data[0].harga_satuan);
      })
}
function total_harga_satuan()
{
  var harga_barang = $('#harga_satuan').val();
  var jumlah = $('#jumlah').val();
  var total_harga_satuan = harga_barang * jumlah;
  document.getElementById('total_harga').value = total_harga_satuan;
}


//awal datatable transaksi
$(function () {
  var id = document.getElementById('transaksi_pembelian_id').value;
  var url = '{{ route("transaksiBarangajax", ":id") }}';
  url = url.replace(':id', id);
  var table = $('.data-table').DataTable({
      ajax: url,
      columns: [
          {data: 'master_barang.nama_barang', name: 'master_barang.nama_barang'},
          {data: 'jumlah', name: 'jumlah'},
          {data: 'harga_satuan', name: 'harga_satuan'},
      ]
  });

  $("#saveBtn" ).click(function() {
      table.ajax.reload(null, false);
  }); 
});
//akhir datatable transaksi

//awal tambah
$("#saveBtn").click(function(event){
event.preventDefault();
var transaksi_pembelian_id = $('#transaksi_pembelian_id').val();
var master_barang_id = $('#master_barang_id').val();
var jumlah = $('#jumlah').val();
var harga_satuan = $('#harga_satuan').val();
let _token   = $('meta[name="csrf-token"]').attr('content');
$.ajax({
  url: "{{ route('transaksiBarangStore') }}",
  type:"POST",
  data:{
    transaksi_pembelian_id:transaksi_pembelian_id,
    master_barang_id:master_barang_id,
    jumlah:jumlah,
    harga_satuan:harga_satuan,
    _token: _token
  },
  success:function(response){
    console.log(response);
    if(response) {
      $('.success').text(response.success);
      const res = response;
      const obj = JSON.parse(res);
      console.log(obj);
      const formatRupiah = (money) => {
        return new Intl.NumberFormat('id-ID',
          { style: 'currency', currency: 'IDR', minimumFractionDigits: 0 }
        ).format(money);
      }
      $('#total_harga_tampil').val(formatRupiah(obj.transaksi.tambahan_total_harga));
    }
    },
    error: function(error) {
      console.log(error);
      document.getElementById("id_barangError").textContent= error.responseJSON.errors.id_barang;
    }
  });
});
//akhir tamba

</script>

@endpush