@extends('layouts.app')

@section('content')
<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">User</h1>
    </div>
    <div class="card">
        <div class="card-header">
            <div class="row">
                <div class="col-lg-6 align-content-between" >
                    <a class="btn btn-outline-primary btn-md" id="createNewBarang">Tambah Data Barang</a>
                </div>
                <div class="col-lg-6 align-content-lg-end" >
                    <table border="0" cellspacing="5" cellpadding="5">
                        <tbody
                            ><tr>
                                <td>Harga Min:</td>
                                <td><input type="text" id="min" name="min"></td>
                            </tr>
                            <tr>
                                <td>Harga Max:</td>
                                <td><input type="text" id="max" name="max"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
        <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-hover mt-3 display" id="dataTable" width="100%" cellspacing="0">
            <thead class="bg-gray-600 text-white">
                <tr>
                    <th>No</th>
                    <th>Nama Barang</th>
                    <th>Harga Satuan</th>
                    <th style="text-align:center; vertical-align: inherit; width: 150px;">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($masterbarang as $key => $item)
                    <tr>
                        <td>{{ $key + 1 }}</td>
                        <td>{{ $item->nama_barang }}</td>
                        <td>{{ ($item->harga_satuan) }}</td>
                        <td>
                            <div class="d-flex flex-wrap flex-column flex-md-row justify-center">
                                <a id="detailBarang" class="btn btn-info btn-detail btn-sm m-1" data-id="{{ $item->id }}" data-toggle="modal" data-target="#modal-id">
                                    <i class="fa fa-info-circle" aria-hidden="true"></i>
                                </a>
                                <a id="updateBarang" class="btn btn-warning btn-sm m-1" data-id="{{ $item->id }}" data-toggle="modal" data-target="#modal-id">
                                    <i class="fa fa-edit" aria-hidden="true"></i>
                                </a>
                                <a id="hapusBarang" class="btn btn-info btn-danger btn-sm m-1" data-id="{{ $item->id }}">
                                    <i class="fa fa-trash fa-sm"></i>
                                </a>
                            </div>  
                        </td>
                    </tr>
                @empty
                    <p>Data User Kosong!</p>
                @endforelse
            </tbody>
            <tfoot>
            </tfoot>
        </table>
    </div>
    </div>
</div>
</div>
<!-- /.container-fluid -->


<!-- Modal Input Data -->
<div class="modal fade" id="modal-id">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="barangCrudModal"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form id="barang">
                <input type="hidden" id="barang_id" name="barang_id" value="">
                <div class="form-group">
                    <label for="nama_barang" class="col-form-label">Nama Barang :</label>
                    <input type="text" class="form-control" id="nama_barang"  name="nama_barang">
                    <span class="text-danger" id="nama_barangError"></span>
                </div>
                <div class="form-group">
                    <label for="harga_satuan" class="col-form-label">Harga Satuan :</label>
                    <input type="text" class="form-control" id="harga_satuan" name="harga_satuan">
                    <span class="text-danger" id="harga_satuanError"></span>
                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary show_confirm" id="submit" value="Submit"></button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- /.Modal Input Data -->
@endsection

@push('scripts')

<link href="{{ asset('sbadmin/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
<script src="{{ asset('sbadmin/vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('sbadmin/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>
{{-- sweet altert --}}
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<script>
// Custom filtering function which will search data in column four between two values
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var min = parseFloat( $('#min').val(), 10 );
        var max = parseFloat( $('#max').val(), 10 );
        var harga = parseFloat( data[2] ) || 0; // use data for the harga column
 
        if ( ( isNaN( min ) && isNaN( max ) ) ||
             ( isNaN( min ) && harga <= max ) ||
             ( min <= harga   && isNaN( max ) ) ||
             ( min <= harga   && harga <= max ) )
        {
            return true;
        }
        return false;
    }
);
$(document).ready(function() {
    // DataTables initialisation
    var table = $('#dataTable').DataTable();

    // Refilter the table
    $('#min, #max').on('change', function () {
        table.draw();
    });

    //modal close
    // awal create barang
    $('#createNewBarang').on('click', function(e) {
        e.preventDefault;
        $('#barangCrudModal').html("Tambah Data Barang");
        document.getElementById("submit").innerHTML = '<i class="fa fa-save" aria-hidden="true"></i> Simpan Data';
        $('#modal-id').modal('show');
        $('#barang_id').val('');
        $('#barang').trigger("reset");
    });

    $('#submit').on('click', function() {
        var id = $("#barang_id").val();
        var nama_barang = $('#nama_barang').val();
        var harga_satuan = $('#harga_satuan').val();

        if(nama_barang!="" && harga_satuan!="" ){
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url: "{{ route('barangStore') }}",
                type: "POST",
                data: {
                    id: id,
                    nama_barang: nama_barang,
                    harga_satuan: harga_satuan
                },
                cache: false,
                success: function(data){
                    console.log(data);
                    var data = JSON.parse(data);
                    if(data.statusCode==200){
                            console.log('done');
                            location.href = "{{ url('/home/barang') }}";
                        }
                        else if(data.statusCode==201){
                            alert("Error occured !");
                        } 
                },
                error:function (response) {
                    document.getElementById("harga_satuanError").textContent= response.responseJSON.errors.harga_satuan;
                }
            });
        }
        else{
            alert('Please fill all the field !');
        }
    });
    // akhir create barang

    // awal edit barang
    $('body').on('click', '#updateBarang', function (event) {
        event.preventDefault();
        var id = $(this).data('id');
        var url = '{{ route("barangEdit", ":id") }}';
        url = url.replace(':id', id);
        $.get(url, function (data) {
            $('#barangCrudModal').html("Update Data Barang");
            document.getElementById("submit").innerHTML = '<i class="fa fa-edit" aria-hidden="true"></i> Update Data';
            $('#modal-id').modal('show');
            $('#barang_id').val(data.data.id);
            $('#nama_barang').val(data.data.nama_barang);
            $('#harga_satuan').val(data.data.harga_satuan);  
        })
    });
    // akhir edit barang

    //awal hapus data
    $('body').on('click', '#hapusBarang', function(event) {
        
        event.preventDefault();
        
        var id = $(this).data('id');    
        Swal.fire({
            title   : "Benar akan menghapus data ini?",
            text    : "Jika Benar maka data akan hilang..!",
            type    : "warning",
            showCancelButton    : true,
            confirmButtonColor  : '#DD6B55',
            confirmButtonText   : "Ya, Hapus aja.!"
        }).then(function (e) {

if (e.value === true) {
    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        type: 'POST',
        url: "{{url('/home/barang')}}/" + id,
        data: {_token: CSRF_TOKEN, _method: 'delete'},
        dataType: 'JSON',
        success: function (results) {
            if (results.success === true) {
                Swal.fire("Error!", results.message, "error");
            } else {
                Swal.fire("Done!", results.message, "success");
                location.href = "{{ url('/home/barang') }}";
            }
        }
    });
        } else {
            e.dismiss;
        }

        }, function (dismiss) {
        return false;
        })

    });
    //akhir hapus data

    //awal detail
    $("#detailBarang").click(function() {
       $("#submit").remove();
       document.getElementById("nama_barang").disabled = true;
       document.getElementById("harga_satuan").disabled = true;

     })

    $('body').on('click', '#detailBarang', function (event) {
        event.preventDefault();
        var id = $(this).data('id');
        var url = '{{ route("barangEdit", ":id") }}';
        url = url.replace(':id', id);
        $.get(url, function (data) {
            $('#barangCrudModal').html("Informasi Data Barang");
            $('#modal-id').modal('show');
            $('#barang_id').val(data.data.id);
            $('#nama_barang').val(data.data.nama_barang);
            $('#harga_satuan').val(data.data.harga_satuan);  
        })
    });

    $('#modal-id').on('hide.bs.modal', function() {
        location.href = "{{ url('/home/barang') }}";
    });
    //akhir detail
});
</script>

@endpush