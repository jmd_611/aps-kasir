<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PenggunaController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MasterBarangController;
use App\Http\Controllers\TransaksiPembelianBarangController;
use App\Http\Controllers\TransaksiPembelianController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::middleware('admin')->group(function(){
    
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    //Manage Data Pengguna
    Route::controller(PenggunaController::class)->group(function () {
        Route::get('/home/pengguna', 'index')->name('penggunaIndex');
        Route::get('/home/pengguna/create', 'create')->name('penggunaCreate');
        Route::post('/home/pengguna', 'store')->name('penggunaStore');
        Route::get('/home/pengguna/{id}', 'show')->name('penggunaShow');
        Route::get('/home/pengguna/{id}/edit', 'show')->name('penggunaEdit');
        Route::put('/home/pengguna/{id}', 'update')->name('penggunaUpdate');
        Route::delete('/home/pengguna/{id}', 'destroy')->name('penggunaDestroy');
    });

    //Manage Data Master Barang
    Route::controller(MasterBarangController::class)->group(function () {
        Route::get('/home/barang', 'index')->name('barangIndex');
        Route::get('/home/barang/create', 'create')->name('barangCreate');
        Route::post('/home/barang', 'store')->name('barangStore');
        Route::get('/home/barang/{id}/edit', 'edit')->name('barangEdit');
        Route::delete('/home/barang/{id}', 'destroy')->name('barangDestroy');
        Route::get('/home/barang/id', 'dataidbarang')->name('barangAjaxId');
        Route::get('/home/barang/harga/{id}', 'datahargabarang')->name('barangAjaxHarga');
    });

    Route::controller(TransaksiPembelianController::class)->group(function () {
        Route::get('/home/transaksi', 'index')->name('transaksiIndex');
        Route::get('/home/transaksi/create', 'create')->name('transaksiCreate');
        Route::post('/home/transaksi', 'store')->name('transaksiStore');
        Route::get('/home/transaksi/{id}/edit', 'edit')->name('transaksiEdit');
        Route::delete('/home/transaksi/{id}', 'destroy')->name('transaksiDestroy');
        Route::get('/home/transaksi/{id}/totalharga', 'getTotalHarga')->name('transaksiTotalHarga');
    });

    Route::controller(TransaksiPembelianBarangController::class)->group(function () {
        Route::get('/home/transaksi/{id}/ajax', 'ajax')->name('transaksiBarangajax');
        Route::get('/home/transaksi/create', 'create')->name('transaksiBarangCreate');
        Route::post('/home/transaksi/barang', 'store')->name('transaksiBarangStore');
        Route::delete('/home/transaksi/barang/{id}', 'destroy')->name('transaksiBarangDestroy');
    });

});

Route::middleware('kasir')->group(function(){
    
});


