<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Str;

class PenggunaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users  = User::all();
        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $roles   = Role::all();
        return view('user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $dataValidation = $request->validate(
                            [
                                'name'                  => 'required',
                                'username'              => 'required|unique:users',
                                'email'                 => 'required|email|unique:users',
                                'password'              => 'required|min:8|confirmed',
                                'password_confirmation' => 'required|min:8',
                                'role_id'               => 'required',
                            ],
                            //custom validation error
                            [
                                'name.required'         => 'Kolom nama di isi dulu lur..!',
                                'username.required'     => 'Kolom username di isi dulu lur..!',
                                'email.required'        => 'Kolom email di isi dulu lur..!',
                                'password.required'     => 'Kolom password di isi dulu lur..!',
                                'password_confirmation.required'     => 'Kolom password confirmation di isi dulu lur..!',
                                'role_id.required'      => 'Kolom Role di pilih dulu lur..!',
                                'username.unique'       => 'Username cari yang lain deh, kayaknya udah kepake lur..!',
                                'email.unique'          => 'Email cari yang lain deh, kayaknya udah kepake lur..!',
                                'password.min'          => 'Kolom password minimal 8 karakter lur..!',
                                'email.email'           => 'Kolom nya harus berupa alamat email lur..!',
                            ]
                        );
        
        $dataValidation['password']     = bcrypt($dataValidation['password']);
        $dataValidation['name']         = Str::title($dataValidation['name']);
        $dataValidation['username']     = Str::lower(Str::replace(' ', '',  $dataValidation['username']));

        $user = User::create($dataValidation);

        return redirect('/home/pengguna');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $roles = Role::all();
        $user = User::findOrFail($id);
        return view('user.show', compact('user', 'roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $roles = Role::all();
        $user = User::findOrFail($id);
        return view('user.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $dataValidation = $request->validate(
            [
                'name'                  => 'required',
                'username'              => 'required',
                'email'                 => 'required|email',
                'role_id'               => 'required',
            ],
            //custom validation error
            [
                'name.required'         => 'Kolom nama di isi dulu lur..!',
                'username.required'     => 'Kolom username di isi dulu lur..!',
                'email.required'        => 'Kolom email di isi dulu lur..!',
                'role_id.required'      => 'Kolom Role di pilih dulu lur..!',
                'email.email'           => 'Kolom nya harus berupa alamat email lur..!',
            ]
        );

        $dataValidation['name']         = Str::title($dataValidation['name']);
        $dataValidation['username']     = Str::lower(Str::replace(' ', '',  $dataValidation['username']));
        $user = User::findOrFail($id);
        $user->fill($dataValidation);
        $user->update();

        return redirect('/home/pengguna');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::findOrFail($id);
        $user->delete();
        return redirect('/home/pengguna');
    }
}
