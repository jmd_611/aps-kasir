<?php

namespace App\Http\Controllers;

use App\Models\TransaksiPembelian;
use App\Http\Requests\StoreTransansiPembelianRequest;
use App\Http\Requests\UpdateTransansiPembelianRequest;
use DB;

class TransaksiPembelianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $transaksi = TransaksiPembelian::where('total_harga' ,'!=', '0')->get();
        return view('transaksi.index', compact('transaksi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTransansiPembelianRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TransansiPembelian  $transansiPembelian
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TransansiPembelian  $transansiPembelian
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTransansiPembelianRequest  $request
     * @param  \App\Models\TransansiPembelian  $transansiPembelian
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TransansiPembelian  $transansiPembelian
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getTotalHarga($id)
    {
        $transaksipembelian = TransaksiPembelian::find($id);
        return response()->json([
            'data'  => $transaksipembelian
        ]);
    }
}
