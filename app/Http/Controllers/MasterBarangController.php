<?php

namespace App\Http\Controllers;

use App\Models\MasterBarang;
use Illuminate\Http\Request;

class MasterBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $masterbarang = MasterBarang::all();
        return view('masterbarang.index', compact('masterbarang'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //        
        $dataValidation = $request->validate(
            [
                'nama_barang'   => 'required',
                'harga_satuan'   => 'required|numeric', 
            ],
            [
                'nama_barang.required'         => 'Kolom nama barang di isi dulu lur..!',
                'harga_satuan.required'         => 'Kolom harga barang di isi dulu lur..!',
                'harga_satuan.numberic'         => 'Kolom harga barang di isi pake angka/harga barang lur..!',
        ]);
        MasterBarang::updateOrCreate(
            [
                'id' => $request->id
            ],
            [
                'nama_barang' => $request->nama_barang,
                'harga_satuan' => $request->harga_satuan
            ]
        );        
        return json_encode(array(
            "statusCode"=>200
        ));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MasterBarang  $masterBarang
     * @return \Illuminate\Http\Response
     */
    public function show(MasterBarang $masterBarang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MasterBarang  $masterBarang
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $barang = MasterBarang::find($id);
        return response()->json([
            'data'  => $barang
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MasterBarang  $masterBarang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MasterBarang $masterBarang)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MasterBarang  $masterBarang
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        MasterBarang::where('id',$id)->delete();
        return response()->json([
            'message' => 'Data deleted successfully!'
          ]);
    }

    public function dataidbarang(Request $request)
    {
        $data   = [];
        if ($request->has('q'))
        {
            $search = $request->q;
            $data   = MasterBarang::select("id", "nama_barang")
                                    ->where("nama_barang", "LIKE", "%$search%")
                                    ->get();
        }
        return response()->json($data);
    }

    public function datahargabarang($request)
    {
        $data = [];
        if ($request !== '')
        {
            $data = MasterBarang::select("id", "harga_satuan")
            ->where("id",$request)
            ->get();
        } else
        {
            $data = [];
        }
        return response()->json($data);
    }
}
