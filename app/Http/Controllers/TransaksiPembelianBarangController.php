<?php

namespace App\Http\Controllers;

use App\Models\TransaksiPembelianBarang;
use App\Models\TransaksiPembelian;
use App\Http\Requests\StoreTransaksiPembelianBarangRequest;
use App\Http\Requests\UpdateTransaksiPembelianBarangRequest;
use App\Models\MasterBarang;
use Illuminate\Http\Request;
use DB;

class TransaksiPembelianBarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(request()->ajax()) {
            return datatables()->of(Company::select('*'))
            ->addColumn('action', 'company-action')
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
        return view('transaksi.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $transaksipembelian_id   = DB::table('transaksi_pembelian')->insertGetId(['total_harga' => '0']);
        $totaltransaksipembelian = TransaksiPembelian::firstWhere('id', $transaksipembelian_id);
        $listtransaksibarangs     = TransaksiPembelianBarang::where('id', $transaksipembelian_id)->get();
        return view('transaksi.create', compact('transaksipembelian_id','totaltransaksipembelian','listtransaksibarangs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTransaksiPembelianBarangRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $dataValidation = $request->validate(
            [
                'master_barang_id'   => 'required',
            ],
            [
                'master_barang_id.required'         => 'Pilih dulu barangnya dulu lur..!',
        ]);
        $data = TransaksiPembelianBarang::updateOrCreate(
            [
                'id' => $request->id
            ],
            [
                'transaksi_pembelian_id' => $request->transaksi_pembelian_id,
                'master_barang_id'      => $request->master_barang_id,
                'jumlah'                => $request->jumlah,
                'harga_satuan'          => $request->harga_satuan
            ]
        );
        
        $transaksi_pembelian_id         = $request->transaksi_pembelian_id;
        $total_harga_sebelumnya         = TransaksiPembelian::select('total_harga')->where('id', $transaksi_pembelian_id)->get();
        $total_harga_sebelumnya_int     = (int)$total_harga_sebelumnya[0]->total_harga;
        $harga_satuan                   = (int)$request->harga_satuan;
        $jumlah                         = (int)$request->jumlah;
        $tambahan_total_harga           =  ($harga_satuan * $jumlah) + $total_harga_sebelumnya_int;

        $transaksi_pembelian = TransaksiPembelian::find($request->transaksi_pembelian_id);
        $transaksi_pembelian->total_harga = $tambahan_total_harga;
        $transaksi_pembelian->update();
        return json_encode(array(
            "statusCode"=>200, "transaksi" => compact('tambahan_total_harga', 'data')
        ));
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TransaksiPembelianBarang  $transaksiPembelianBarang
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TransaksiPembelianBarang  $transaksiPembelianBarang
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTransaksiPembelianBarangRequest  $request
     * @param  \App\Models\TransaksiPembelianBarang  $transaksiPembelianBarang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TransaksiPembelianBarang  $transaksiPembelianBarang
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
    }

    public function ajax($id)
    {
        //
        $barang = TransaksiPembelianBarang::where('transaksi_pembelian_id', $id)
                                            ->with('masterBarang:id,nama_barang')
                                            ->get();
        return response()->json([
            'data'  => $barang
        ]);
    }
}
