<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterBarang extends Model
{
    use HasFactory;
    protected $table  = 'master_barang';
    protected $fillable  = [
        'nama_barang', 'harga_satuan'
    ];
    public function transaksiPembelianBarang()
    {
        return $this->hasMany(TransaksiPembelianBarang::class);
    }
    

}
