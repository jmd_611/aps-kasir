<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Blade::directive('currency', function ( $expression ){ 
            return "Rp. <?php echo number_format($expression,0,',','.'); ?>"; 
        });
        Gate::define('admin', function($user) {
            return Str::lower($user->role->nama) === 'admin';
        });
        Gate::define('kasir', function($user) {
            return Str::lower($user->role->nama) === 'kasir';
        });
    }
}
