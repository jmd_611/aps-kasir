<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $role   = array(
            [
                'id'        => '1',
                'nama'      => 'admin',
            ],
            [
                'id'        => '2',
                'nama'      => 'kasir',
            ]
        );
        DB::table('role')->insert($role);
    }
}
