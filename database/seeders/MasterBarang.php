<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MasterBarang extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $masterBarang   = array(
            [
                'id'                => '1',
                'nama_barang'       => 'Sabun batang',
                'harga_satuan'      => '1000',
            ],
            [
                'id'                => '2',
                'nama_barang'       => 'Mi Instan',
                'harga_satuan'      => '2000',
            ],
            [
                'id'                => '3',
                'nama_barang'       => 'Pensil',
                'harga_satuan'      => '1000',
            ],
            [
                'id'                => '4',
                'nama_barang'       => 'Kopi sachet',
                'harga_satuan'      => '1500',
            ],
            [
                'id'                => '5',
                'nama_barang'       => 'Air minum galon',
                'harga_satuan'      => '20000',
            ],
        );

        DB::table('master_barang')->insert($masterBarang);
    }
}
