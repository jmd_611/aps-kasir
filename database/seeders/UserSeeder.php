<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert([
            'name'      => Str::lower('Ahmad Jumadi'),
            'username'  => Str::lower('admin'),
            'email'     => Str::lower('admin@jum.my.id'),
            'password'  => Hash::make('12345678'),
            'role_id'   => '1',
        ]);
    }
}
